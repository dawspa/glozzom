# Glozzom

Multi-page business site mockup made with _Bootstrap 4_

![Glozzom screenshot](img/glozzom.png)

## Access

https://glozzom-ds.netlify.com/

## License

Standard MIT License

made with Brad Traversy's [Udemy course](https://www.udemy.com/bootstrap-4-from-scratch-with-5-projects/)

**Thank You for viewing my project!**
